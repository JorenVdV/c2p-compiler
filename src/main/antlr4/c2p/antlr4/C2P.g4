grammar C2P;

parse : compilation_unit EOF;

compilation_unit : (include | declaration | function_declaration)*;

include: Include ('<' file=Identifier Dot extension=Identifier '>' | '"' file=Identifier Dot extension=Identifier '"');

declaration : t=type cd =complex_decl Semi;

type: Const? t=Char| Const? t=Int| Const? t=Void;

complex_decl : variable (Comma variable)*;

variable : Star? name =variable_id (Assign expr=expression)?;

variable_id : id=Identifier (ai=array_index)?;

array_index : LeftBracket (Number | Identifier) RightBracket;

function_declaration : t=type funcname=Identifier LeftParen plist=parameter_list RightParen ((LeftBrace blck=block RightBrace)|Semi);

parameter_list : (parameter (Comma parameter)*)? | Void;

parameter : type Identifier;

block : statement* ;

statement : if_statement
		| while_statement
		| for_statement
		| expression
		| return_statement
		| declaration; // might still change


if_statement : If LeftParen expression RightParen ((statement|(Continue|Break) Semi)| LeftBrace block RightBrace) else_statement?;

else_statement : Else (statement| Continue|Break| LeftBrace block RightBrace);

while_statement : While LeftParen expression RightParen (statement| LeftBrace block RightBrace);

for_statement : For LeftParen expression Semi expression Semi expression RightParen (statement| LeftBrace block RightBrace);

return_statement: Return (expression)?;

expression : (pf_expr Assign)? or_expr (Semi)?;
		
or_expr: and_expr (OrOr and_expr)*;

and_expr: rel_expr (AndAnd rel_expr)*;

rel_expr: add_expr
		| add_expr (Less| LessEqual | Greater | GreaterEqual| Equal |NotEqual) add_expr;
		
add_expr: mult_expr ( (Plus| Minus) add_expr)?;

mult_expr: unary_expr ( (Star | Div) mult_expr)?;

unary_expr: (Minus|Not)? pf_expr;

pf_expr: pf_expr Dot Identifier
	| Identifier LeftParen arg_list RightParen
	|pr_expr;
	
arg_list: expression ((Comma expression)*)?;

pr_expr : Identifier (array_index)? (PlusPlus | MinusMinus)?
	| Number
	| String
	| Character
	| LeftParen expression RightParen
	| LeftBrace arg_list RightBrace // Array assignments
	;


/* Lexer rules */
Break : 'break';
Char : 'char';
Const : 'const';
Continue : 'continue';
Else : 'else';
For : 'for';
If : 'if';
Int : 'int';
Return : 'return';
Void : 'void';
While : 'while';
LeftParen : '(';
RightParen : ')';
LeftBracket : '[';
RightBracket : ']';
LeftBrace : '{';
RightBrace : '}';
Less : '<';
LessEqual : '<=';
Greater : '>';
GreaterEqual : '>=';
Plus : '+';
PlusPlus : '++';
Minus : '-';
MinusMinus : '--';
Star : '*';
Div : '/';
Mod : '%';
AndAnd : '&&';
OrOr : '||';
Caret : '^';
Not : '!';
Semi : ';';
Comma : ',';
Assign : '=';
StarAssign : '*=';
DivAssign : '/=';
ModAssign : '%=';
PlusAssign : '+=';
MinusAssign : '-=';
Equal : '==';
NotEqual : '!=';
Arrow : '->';
Dot : '.';
Include : '#include';


Identifier : Nondigit (Digit | Nondigit)*;
Number : Digit+;
String : '\"'.*?'\"'; //Non greedy lookahead
Character : '\''.'\'';

fragment Digit : [0-9];
fragment Nondigit : [A-Za-z_];

Whitespace: [ \t]+ -> channel(HIDDEN);
Newline: ( '\r' '\n'?| '\n') -> channel(HIDDEN);
BlockComment : '/*' .*? '*/'-> skip;
LineComment: '//' ~[\r\n]*-> skip;
