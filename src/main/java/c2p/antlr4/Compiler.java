package main.java.c2p.antlr4;

import main.java.c2p.antlr4.ast.ASTnode;
import main.java.c2p.antlr4.ast.C2PCustomVisitor;
import main.java.c2p.antlr4.ast.nodes.CompilationUnit;

import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.tree.ParseTree;

import c2p.antlr4.C2PLexer;
import c2p.antlr4.C2PParser;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.*;

public class Compiler {

	/*
	 * Main compiler class. Should be called by using the c2p command. argv[0]
	 * contains the program to compile argv[1..n] contain flags, to be
	 * determined later.
	 * 
	 * Here we should read the file, construct the AST, Return the program. This
	 * should be the only main in the project!
	 */
	public static void main(String[] argv) throws Exception {
		String filetoparse = "";
		String output_file = "";
		Vector<String> flags = new Vector<String>();

		boolean writeTextAST = false;
		boolean writeTreeAST = false;
		boolean gui = false;

		if (argv.length > 0) {
			// File to parse should always be the first argument.
			filetoparse = argv[0];
			// File to write to should always be the second argument
			output_file = argv[1];
//			System.out.println("Starting the parsing of file:" + filetoparse
//					+ ".");
			// Read all flags
			for (int i = 2; i < argv.length; i++) {
				if (argv[i].equals("-writeTextAST"))
					writeTextAST = true;
				else if (argv[i].equals("-writeTreeAST"))
					writeTreeAST = true;
				else if (argv[i].equals("-gui"))
					gui = true;
				else
					System.err.println("Encountered Illegal Flag.");
			}
		}

		// Errors on input
		File infile = new File(filetoparse);
		if (!infile.exists() || filetoparse.equals("")) {
			System.err.println("File to parse was not found");
			return;
		}
		if (output_file.equals("") || output_file.charAt(0) == '-') {
			System.err.println("No viable output file was given");
			return;
		}

		C2PLexer lexer = new C2PLexer(new ANTLRFileStream(filetoparse));
		C2PParser parser = new C2PParser(new CommonTokenStream(lexer));
		ParseTree tree = parser.parse();

		C2PCustomVisitor visitor = new C2PCustomVisitor();
		CompilationUnit comp = (CompilationUnit) visitor.visit(tree);
		
		if(comp == null)return;
		
		if(writeTextAST){
			comp.print("");
		}
		if(writeTreeAST){
			File treefile = new File("tree.gv");
			FileOutputStream treeout = new FileOutputStream(treefile);
			BufferedWriter tw = new BufferedWriter(new OutputStreamWriter(treeout));
			comp.writeTree(tw);
			
			tw.flush();
			tw.close();
		}
		if(gui){
			((RuleContext) tree).inspect(parser);
		}

		File fout = new File(output_file);
		FileOutputStream fos = new FileOutputStream(fout);
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

		comp.generateCode(bw);

		bw.flush();
		bw.close();

	}
}