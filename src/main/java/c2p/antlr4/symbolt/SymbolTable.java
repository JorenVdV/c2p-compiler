package main.java.c2p.antlr4.symbolt;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class SymbolTable {
	// Class containing the renewed Symbol table

	/* ============================================ */
	/* Data members */
	/* ============================================ */
	/* data member: list of all declared variables */
	private HashMap<Integer, Scope> _scopes;
	/* data member: current scope */
	private Scope _current_scope;
	/* data member: reserver words */
	private Set<String> _reserved_words;

	/* ============================================ */
	/* Member functions */
	/* ============================================ */

	public SymbolTable()
	/**
	 * Constructor, sets the reserverd word list, initializes the scopes list
	 * and sets the current scope to null
	 */
	{
		String[] array = { "if", "while", "for", "else", "return", "int",
				"char", "break", "continue" };
		_reserved_words = new HashSet<String>(Arrays.asList(array));
		_scopes = new HashMap<Integer, Scope>();
		_current_scope = null;
	}

	public void AddNewScope(int scopeid) throws Exception
	/**
	 * Adds a new scope to the symboltable.
	 * 
	 * @param the
	 *            id of the new scope
	 * @throws exception
	 *             when scope with same scopeid already exists
	 */
	{

		/* check if the scope is unique */
		if (_scopes.containsKey(scopeid)) {
			System.err.println("Trying to add scope with id :" + scopeid
					+ ", but it already exists. Throwing exception");
			throw new Exception();
		}
		/* construct the new scope, set is as current and add to scope list. */
		Scope newscope = new Scope(scopeid, _current_scope);
		_current_scope = newscope;
		_scopes.put(scopeid, newscope);
	}

	public void LeaveScope() throws Exception
	/**
	 * Leaves the current scope and returns to its parent scope
	 * 
	 * @throws exception
	 *             when the current scope is null
	 */
	{
		/* check for availability current scope */
		if (_current_scope == null) {
			System.err
					.println("Trying to leave non-existing scope. Throwing exception");
			throw new Exception();
		}
		_current_scope = _current_scope.getParent();
	}

	public void Addsymbol(String name, Type type) throws Exception
	/**
	 * Adds a new Symbol to the symbol table
	 * 
	 * @param name
	 *            , name of the variable
	 * @param type
	 *            , type of the variable
	 * @throws exception
	 *             when the current scope is null
	 * @throws exception
	 *             when the name is one of the reserved words
	 * @throws exception
	 *             when the name is already declared
	 * @future TODO maybe add lines to the symbols for reference (e.g. first
	 *         declared here line 4)
	 */
	{
		/* check if the current scope is not 0 */
		if (_current_scope == null) {
			System.err
					.println("Trying to add symbol to empty symbol table, initialize first");
			throw new Exception("USAGE");
		}
		/* check if the name is not a reserved word */
		if (_reserved_words.contains(name)) {
			System.err.println("Variable " + name + " is not allowed, " + name
					+ " is reserved");
			throw new Exception("RESERVEDWORD");
		}
		/* check if the name is not already declared */
		if (_current_scope.Contains(name)) {
			System.err.println("Duplicate symbol name: " + name);
			throw new Exception("DUPLICATE");
		}
		_current_scope.AddSymbol(name, type);

	}
	
	public void RemoveSymbol(String name) throws Exception
	/**
	 * Removes the symbol with the given name from the symboltable
	 * @param name, the name of the symbol we need to remove
	 * @throws excepton when the current scope is null
	 * @throws exception when the symbol is not in a scope
	 */
	{	
		/* check if the current scope is not 0 */
		if (_current_scope == null) {
			System.err
					.println("Trying to add symbol to empty symbol table, initialize first");
			throw new Exception("USAGE");
		}
		/* check if the name is not already declared */
		if (!_current_scope.Contains(name)) {
			System.err.println("Symbol not found: " + name);
			throw new Exception("ILLEGALRELEASE");
		}
		
		_current_scope.RemoveSymbol(name);	
	}
	
	

}
