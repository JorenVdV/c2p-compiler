package main.java.c2p.antlr4.symbolt;

public enum Type {
	CHAR, INT, VOID, ERROR
}
