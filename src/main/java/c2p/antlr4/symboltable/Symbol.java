package main.java.c2p.antlr4.symboltable;

public class Symbol {

	private String _name; //Name of the variable
	private Type _type; //Type of the variable
	private long _address;
	
	public Symbol(){
		
	}
	
	public Symbol(String n, Type t){
		this._name = n;
		this._type = t;
	}
	
	public Symbol(String n, Type t, long addr){
		this._name = n;
		this._type = t;
		this._address = addr;
	}
	public String getName() {
		return _name;
	}
	public void setName(String name) {
		this._name = name;
	}
	public Type getType() {
		return _type;
	}
	public void setType(Type type) {
		this._type = type;
	}
}
