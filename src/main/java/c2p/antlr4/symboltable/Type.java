package main.java.c2p.antlr4.symboltable;

public class Type {
		public enum BaseType {
			INT, CHAR, VOID
		}
		
		private BaseType _type;
		// private long int _address; // Moved to symbol
		
		public Type(BaseType t){
			this._type = t;
		}
		
		public BaseType getType(){
			return _type;
		}
		
		public String toString()
		{
			switch (_type) {
				case INT: return "int";
				case CHAR:return "char";
				case VOID:return "void";
				default: return "error";
			}
		}
}
