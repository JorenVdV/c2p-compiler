package main.java.c2p.antlr4.symboltable;

public class VariableSymbol extends Symbol {
	private String _value; //Value of the variable, either a string, or an integer in string format
	
	public VariableSymbol(String name, Type type, String value){
		super(name, type);
		_value = value;
		
	}

	public String get_value() {
		return _value;
	}

	public void set_value(String _value) {
		this._value = _value;
	}

}
