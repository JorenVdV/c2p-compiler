package main.java.c2p.antlr4.symboltable;

import java.util.*;

public class FunctionSymbol extends Symbol {
	
	private Vector<Pair<Type,String>> _Parameterlist; //Contains N
	
	public FunctionSymbol(String name, Type t, Vector<Pair<Type,String>> params){
		
		super(name, t);
		_Parameterlist = params; 	
		
	};

}
