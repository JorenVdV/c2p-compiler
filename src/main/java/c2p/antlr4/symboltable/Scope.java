package main.java.c2p.antlr4.symboltable;

import java.util.Hashtable;

import main.java.c2p.antlr4.ast.ASTnode;

public class Scope {
	
	private int _id;
	private Scope _parent;
	
	private Hashtable<String, ASTnode> _scopeContents;
	
 
	public Scope() {
		_scopeContents = new Hashtable<String, ASTnode>();
	}
	public Scope(int newid, Scope parent) {
		_id = newid;
		_parent = parent;
		_scopeContents = new Hashtable<String, ASTnode>();
	}
	
	/*
	 * Cannot add Null symbols!!!
	 */
	public void AddSymbol(ASTnode s) throws DuplicateSymbolException{
		if(s != null){
			String name = s.getName();
			if(!_scopeContents.containsKey(name)){
				_scopeContents.put(name, s);
			}else{
				System.out.println("Tried to add duplicate symbol: "+ name);
				throw new DuplicateSymbolException(name);
			}
		}
		else{
			System.out.println("Tried to add null, nothing was added.");
		}
	}
	
	
	public ASTnode GetSymbol(String name) throws Exception{
		if(contains(name)){
			if (_scopeContents.containsKey(name)) return _scopeContents.get(name);
			else if (_parent != null) return _parent.GetSymbol(name);
			else return null;
		}
		else{
			System.out.println("Couldn't find your symbol, getting a Null symbol instead: "+ name);
			throw new Exception();
			//return null;
			
		}
	}
	
	public void RemoveSymbol(String name){	
		if(_scopeContents.containsKey(name)) _scopeContents.remove(name);
		else if (_parent != null && contains(name)) _parent.RemoveSymbol(name);
	}
	
	public boolean contains(String name){
		if(_scopeContents.containsKey(name)){
			return true;
		}
		else{
			if(_parent != null)return _parent.contains(name);
			else return false;
		}
	}


	public int get_id() {
		return _id;
	}


	public void set_id(int _id) {
		this._id = _id;
	}
	
	public Scope getParent(){
		return _parent;
	}
	public int getNOV() {
		return _scopeContents.size();
	}

}
