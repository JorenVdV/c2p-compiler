package main.java.c2p.antlr4.symboltable;

import java.util.Vector;

import main.java.c2p.antlr4.ast.ASTnode;
import main.java.c2p.antlr4.ast.nodes.FunctionDecl;
import main.java.c2p.antlr4.symboltable.Type.BaseType;

public class SymbolTable {
	/*
	 * Our SymbolTable is 1 list of HashTables, each Hashtable represents a specific scope.
	 * 
	 * Ref: http://www.d.umn.edu/~rmaclin/cs5641/Notes/L15_SymbolTable.pdf
	 *
	 */
	private ReservedWords _ReservedWords;
	//Shouldn't there be a tree like structure?
	
	private Vector<Scope> _ScopeList;
	private Scope _current;
	
	public SymbolTable(){
		_ReservedWords = new ReservedWords();
		_ScopeList = new Vector<Scope>(); //Can't use list of Hashtables, so we use Vector
		_current = null;
	}
	
	/*
	 * When adding a new scope , we put it at the front.
	 * Pos 0 = innermost.
	 * Pos n-1 = outermost
	 */
	public void AddNewScope(int scopeid){
		_ScopeList.add(0,new Scope(scopeid, _current));
		_current = _ScopeList.get(0);
//		System.out.println("adding new scope: "+_current.get_id());
	}
	
	public void RemoveScope(int id){
		for( Scope s : _ScopeList){
			if (s.get_id() == id){
				s.set_id(-1);
				_ScopeList.remove(s);
			}
		}
	}
	
	public void LeaveScope(){
//		System.out.println("leaving scope: "+_current.get_id());
		_current = _current.getParent();
//		System.out.println("now in scope: "+_current.get_id());
	}
	
	public void AddSymbol(ASTnode s) throws DuplicateSymbolException{
		_current.AddSymbol(s);
	}
	/*
	 * Crawls the entire Symbol Table and removes the requested symbol with name name
	 * @param name
	 */
	public void RemoveSymbol(String name){
		for(Scope p : _ScopeList){
			if(p.contains(name)){
				p.RemoveSymbol(name);
			}
			else{
				System.out.println("No Deletion, Symbol with name: " + name + ", Not found.");
			}
		}
		
	}
	/*
	 * Looking up an identifier is done by looking at the inner (first) scope.
	 * If it is not found, we dig deeper. If it's not found, we return an error.
	 */
	public ASTnode LookUp(String s) throws Exception{
		if(_current.contains(s))return _current.GetSymbol(s);
		else {
			System.err.println("Could not found Symbol: "+s);
			throw new Exception();
		}
		
	}
	/*
	 * When the include macro is used, we should add these to the symbol table
	 */
	public void AddLibraryReserved() throws DuplicateSymbolException{
		AddSymbol(new FunctionDecl("printf", new Type(BaseType.VOID), false, null));
		AddSymbol(new FunctionDecl("scanf", new Type(BaseType.VOID), false , null));
	}

	public Scope getScope(int id) {
		for(Scope scope: _ScopeList){
			if (scope.get_id() == id) return scope;
		}
		return null;
	}
	
	
}
