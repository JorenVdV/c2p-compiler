package main.java.c2p.antlr4.symboltable;

public class DuplicateSymbolException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public DuplicateSymbolException(String s){
		name = s;
	}
	public String getName(){
		return name;
	}
	private String name;

}
