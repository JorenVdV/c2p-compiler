package main.java.c2p.antlr4.symboltable;

import java.util.*; //Vector Import

public class ReservedWords {
	
	public Vector<String> reserved = new Vector<String>();
	
	public ReservedWords(){
		reserved.add("if");
		reserved.add("while");
		reserved.add("for");
		reserved.add("else");
		reserved.add("return");
		reserved.add("int");
		reserved.add("char");
		reserved.add("break");
		reserved.add("continue");
		
	}
	
	public boolean IsMemberOf(String r){
		return (reserved.contains(r));
	}
	
	public int SizeOf(){
		return reserved.size();
	}
	
	public void AddReservedWord(String r){
		if (!reserved.contains(r)){
			reserved.add(r);
			System.out.println("Added Reserved Word:" + r + ".");
		}
		else{
			System.out.println("Already contained Reserved Word:" + r + " . Added nothing");
		}	
	}
	
	public void RemoveReservedWord(String r){
		reserved.remove(r);
	}

}
