package main.java.c2p.antlr4.ast;

import java.io.IOException;
import java.io.BufferedWriter;

import main.java.c2p.antlr4.symboltable.SymbolTable;


public class ASTnode {
	// type
	private int _type;
	private final int _id;
	public static SymbolTable _symboltable = new SymbolTable();
	/*
	 * This member is declared Static, this means that only 1 instance of this
	 * member will be shared amongst all AST node classes. Use this instance when 
	 * walking over the Tree, to save all variables to.
	 */
	
	public ASTnode(int type){
		_type = type;
		if (_symboltable == null) _symboltable = new SymbolTable();
		_id = IDgenerator.generateID();
	}
	
	public int GetType(){
		return _type;
	}
	
	public int getID(){
		return _id;
	}
	
	public void print(String tab){}
	
	public void writeTree(BufferedWriter w) throws IOException{}
	
	public void generateCode(BufferedWriter w) throws IOException{}

	public String getName() { return null;} 
	
}