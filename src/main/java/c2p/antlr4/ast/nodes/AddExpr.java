package main.java.c2p.antlr4.ast.nodes;

import java.io.BufferedWriter;
import java.io.IOException;

import c2p.antlr4.C2PParser;
import main.java.c2p.antlr4.ast.ASTnode;

public class AddExpr extends ASTnode {
	public enum OpAdd{ PLUS, MINUS }
	
	private ASTnode _expr0;
	private ASTnode _expr1; 
	private OpAdd _op;

	public AddExpr(ASTnode expr0, ASTnode expr1, OpAdd op) {
		super(C2PParser.RULE_add_expr);
		_expr0 = expr0;
		_expr1 = expr1;
		_op = op;
	}
	
	@Override
	public void print(String tab){
		System.out.println(tab+"AddExpr:");
		switch (_op){
		case MINUS:System.out.println(tab+"\tMinus");
			break;
		case PLUS:System.out.println(tab+"\tAdd");
			break;
		default:
			break;
		}
		_expr0.print(tab+"\t");
		_expr1.print(tab+"\t");
		
		
	}
	
	@Override
	public void writeTree(BufferedWriter w) throws IOException
	{
		w.write(getID()+"");
		w.newLine();
		if(_op.equals(OpAdd.PLUS))w.write(getID()+"[label=\"Add\"]");
		if(_op.equals(OpAdd.MINUS))w.write(getID()+"[label=\"Minus\"]");
		w.newLine();
		w.write(getID()+"->");
		_expr0.writeTree(w);
		w.newLine();
		w.write(getID()+"->");
		_expr1.writeTree(w);
		w.newLine();
	}
	
	@Override
	public void generateCode(BufferedWriter w) throws IOException
	{
		_expr0.generateCode(w);
		_expr1.generateCode(w);
		if(_op.equals(OpAdd.PLUS))w.write("add i");
		if(_op.equals(OpAdd.MINUS))w.write("sub i");
	}

}
