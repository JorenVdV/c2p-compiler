package main.java.c2p.antlr4.ast.nodes;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.List;

import c2p.antlr4.C2PParser;
import main.java.c2p.antlr4.ast.ASTnode;

public class Block extends ASTnode {
	private List<Statement> _statements;
	
	public List<Statement> get_statements() {
		return _statements;
	}

	public void set_statements(List<Statement> _statements) {
		this._statements = _statements;
	}


	public Block() {
		super(C2PParser.RULE_block);
		_statements = null;
	}
	
	public void addStatements(List<Statement> stat){
		_statements = stat;
	}
	
	@Override
	public void print(String tab){
		System.out.println(tab+"Block");
		for(Statement stat : _statements){
			stat.print(tab);
		}
	}
	
	@Override
	public void writeTree(BufferedWriter w) throws IOException
	{
		w.write(getID()+"");
		w.newLine();
		w.write(getID()+"[label=\"Block\"]");
		w.newLine();
		for(Statement state:_statements){
			w.write(getID()+"->");
			state.writeTree(w);
			w.newLine();
		}
	}
	
	@Override
	public void generateCode(BufferedWriter w) throws IOException{
		
	}

}
