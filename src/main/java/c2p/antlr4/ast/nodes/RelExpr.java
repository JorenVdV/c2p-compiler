package main.java.c2p.antlr4.ast.nodes;

import java.io.BufferedWriter;
import java.io.IOException;

import c2p.antlr4.C2PParser;
import main.java.c2p.antlr4.ast.ASTnode;

public class RelExpr extends ASTnode {
	public enum Rel{
		LESS, LESSEQ, GREATER, GREATEREQ, EQ, NEQ
	}
	
	private ASTnode _expr0;
	private ASTnode _expr1;
	
	private Rel _rel;

	public RelExpr(ASTnode expr0, ASTnode expr1, Rel rel) {
		super(C2PParser.RULE_rel_expr);
		_expr0 = expr0;
		_expr1 = expr1;
		_rel = rel;
	}
	
	@Override
	public void print(String tab){
		System.out.println(tab+"RelExpr:");
		switch (_rel){
		case EQ: System.out.println(tab+"\tEQUAL");
			break;
		case GREATER:System.out.println(tab+"\tGREATER");
			break;
		case GREATEREQ:System.out.println(tab+"\tGREATEREQ");
			break;
		case LESS:System.out.println(tab+"\tLESS");
			break;
		case LESSEQ:System.out.println(tab+"\tLESSEQ");
			break;
		case NEQ:System.out.println(tab+"\tNOT EQ");
			break;
		default:
			break;
		
		}
		_expr0.print(tab+"\t");
		_expr1.print(tab+"\t");
	}
	
	@Override
	public void writeTree(BufferedWriter w) throws IOException
	{
		w.write(getID()+"");
		w.newLine();
		switch (_rel){
		case EQ: w.write(getID()+"[label=\"Equal\"]");
			break;
		case GREATER:w.write(getID()+"[label=\"Greater\"]");
			break;
		case GREATEREQ:w.write(getID()+"[label=\"Greater Equal\"]");
			break;
		case LESS:w.write(getID()+"[label=\"Less\"]");
			break;
		case LESSEQ:w.write(getID()+"[label=\"Less Equal\"]");
			break;
		case NEQ:w.write(getID()+"[label=\"Not Equal\"]");
			break;
		default:
			break;
		
		}
		w.newLine();
		w.write(getID()+"->");
		_expr0.writeTree(w);
		w.newLine();
		w.write(getID()+"->");
		_expr1.writeTree(w);
		w.newLine();
	}
	
	@Override
	public void generateCode(BufferedWriter w) throws IOException{
		
	}

}
