package main.java.c2p.antlr4.ast.nodes;

import java.io.BufferedWriter;
import java.io.IOException;

import c2p.antlr4.C2PParser;
import main.java.c2p.antlr4.ast.ASTnode;

public class UnaryExpr extends ASTnode {
	private ASTnode _expr;
	private boolean _minus;
	private boolean _not;

	public UnaryExpr(ASTnode expr, boolean minus, boolean not) {
		super(C2PParser.RULE_unary_expr);
		_expr = expr;
		_minus = minus;
		_not = not;
	}
	
	public ASTnode getChild(){
		return _expr;
	}
	
	@Override
	public void print(String tab){
		System.out.println(tab+"UnaryExpr");
		System.out.println(tab+"\tMinus = "+_minus);
		System.out.println(tab+"\tNot = "+_not);
		if(_expr != null)_expr.print(tab+"\t");
	}
	
	@Override
	public void writeTree(BufferedWriter w) throws IOException
	{
		w.write(getID()+"");
		w.newLine();
		w.write(getID()+"[label=\"Unary Expression\"]");
		w.newLine();
		w.write(getID()+"->");
		_expr.writeTree(w);
		w.newLine();
	}
	
	@Override
	public void generateCode(BufferedWriter w) throws IOException{
		
	}

}
