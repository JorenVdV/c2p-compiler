package main.java.c2p.antlr4.ast.nodes;

import java.io.BufferedWriter;
import java.io.IOException;

import c2p.antlr4.C2PParser;
import main.java.c2p.antlr4.ast.ASTnode;

public class Expression extends ASTnode {
	private ASTnode _rvalue;
	private ASTnode _lvalue;
	
	
	public Expression(ASTnode rvalue) {
		super(C2PParser.RULE_expression);
		_rvalue = rvalue;
		_lvalue = null;
	}
	
	public void addLvalue(ASTnode lvalue)
	{
		_lvalue =  lvalue;
	}
	
	public ASTnode getRvalue(){
		return _rvalue;
	}
	
	public boolean isSingleVal(){
		return (_rvalue.getClass().equals(UnaryExpr.class));
	}
	
	@Override
	public void print(String tab){
		System.out.println(tab+"Expression:");
		if(_lvalue != null) _lvalue.print(tab+"\t");
		_rvalue.print(tab+"\t");
	}
	
	@Override
	public void writeTree(BufferedWriter w) throws IOException
	{
		w.write(getID()+"");
		w.newLine();
		w.write(getID()+"[label=\"Expression\"]");
		w.newLine();
		if(_lvalue!=null){
			w.write(getID()+"->");
			_lvalue.writeTree(w);
			w.newLine();
		}
		w.write(getID()+"->");
		_rvalue.writeTree(w);
		w.newLine();
	}
	
	@Override
	public void generateCode(BufferedWriter w) throws IOException
	{
		// Expression itself is only a container so it does not have to generate
		// for the rvalue
		_rvalue.generateCode(w);
		//if the _lvalue is not null there is an assignment
		// -> store value in address space of the variable
		//TODO
		
		
	}
	
	

}
