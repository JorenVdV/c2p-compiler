package main.java.c2p.antlr4.ast.nodes;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;

import main.java.c2p.antlr4.ast.ASTnode;

public class ArgList extends ASTnode {
	ArrayList<ASTnode> _args;
	public ArgList(ArrayList<ASTnode> args) {
		super(32);
		_args = args;
	}
	
	@Override
	public void print(String tab){
		System.out.println(tab+"ArgList:");
		for(ASTnode arg: _args){
			arg.print(tab+"\t");
		}
	}
	
	@Override
	public void writeTree(BufferedWriter w) throws IOException
	{
		w.write(getID()+"");
		w.newLine();
		w.write(getID()+"[label=\"Argument list\"]");
		w.newLine();
		for(ASTnode arg: _args){
			w.write(getID()+"->");
			arg.writeTree(w);
			w.newLine();
		}
	}
	
	@Override
	public void generateCode(BufferedWriter w) throws IOException{
		
	}

}
