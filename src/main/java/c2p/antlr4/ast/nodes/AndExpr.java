package main.java.c2p.antlr4.ast.nodes;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;

import c2p.antlr4.C2PParser;
import main.java.c2p.antlr4.ast.ASTnode;

public class AndExpr extends ASTnode {
	private ArrayList<ASTnode> _exprs;
	
	public AndExpr(ArrayList<ASTnode> exprs) {
		super(C2PParser.RULE_and_expr);
		_exprs = exprs;
	}
	
	@Override
	public void print(String tab){
		System.out.println(tab+"AndExpr:");
		for(ASTnode expr:_exprs){
			expr.print(tab+"\t");
		}
	}
	
	@Override
	public void writeTree(BufferedWriter w) throws IOException
	{
		w.write(getID()+"");
		w.newLine();
		w.write(getID()+"[label=\"And\"]");
		w.newLine();
		for(ASTnode expr: _exprs){
			w.write(getID()+"->");
			expr.writeTree(w);
			w.newLine();
		}
	}
	
	
	@Override
	public void generateCode(BufferedWriter w) throws IOException
	{
		for(ASTnode expr: _exprs){
			expr.generateCode(w);
			if(!expr.equals(_exprs.get(0)))w.write("and");
		}
	}

}
