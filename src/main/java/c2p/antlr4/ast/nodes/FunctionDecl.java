package main.java.c2p.antlr4.ast.nodes;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Vector;

import c2p.antlr4.C2PParser;
import main.java.c2p.antlr4.ast.ASTnode;
import main.java.c2p.antlr4.symboltable.Pair;
import main.java.c2p.antlr4.symboltable.Type;

public class FunctionDecl extends ASTnode {
	String _fname;
	Type _type;
	boolean _constant;
	Vector<Pair<Type, String> > _plist;
	Block _block;

	public FunctionDecl(String name, Type t, boolean contant, 
			Vector<Pair<Type, String> > plist) 
	{
		super(C2PParser.RULE_function_declaration);
		_fname = name;
		_type = t;
		_plist = plist;
		_block = null;
	}
	
	@Override
	public String getName(){
		return _fname;
	}
	
	public void addBlock(Block block)
	{
		_block = block;
	}
	
	@Override
	public void print(String tab)
	{
		System.out.println(tab+"Function:");
		System.out.println(tab+"\tfunctioname = "+_fname);
		System.out.println(tab+"\treturn type = "+_type.toString());
		System.out.println(tab+"\tconstant return type = "+_constant);
		System.out.print(tab+"\tparameter list = {");
		for (Pair<Type, String> par: _plist)
		{
			System.out.print("("+par.getFirst().toString());
			System.out.print(",");
			System.out.print(par.getSecond().toString()+")");
			if(!par.equals(_plist.lastElement()))System.out.println(",");
		}
		System.out.println("}");
		_block.print(tab+"\t");
	}
	
	@Override
	public void writeTree(BufferedWriter w) throws IOException
	{
		w.write(getID()+"");
		w.newLine();
		w.write(getID()+"[label = \"Function "+_fname+"()\"]");
		w.newLine();
		w.write(getID()+"->");
		_block.writeTree(w);
		
	}
	
	@Override
	public void generateCode(BufferedWriter w) throws IOException{
		
	}

}
