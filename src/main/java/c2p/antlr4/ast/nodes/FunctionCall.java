package main.java.c2p.antlr4.ast.nodes;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;

import main.java.c2p.antlr4.ast.ASTnode;

public class FunctionCall extends ASTnode {
	String _identifier;
	private ArrayList<ASTnode > _args;
	
	public FunctionCall(String id, ArrayList<ASTnode> args) {
		super(28);
		_identifier = id;
		_args = args;
	}
	
	@Override
	public void print(String tab){
		System.out.println(tab+"FucntionCall:");
		System.out.println(tab+"\tFunctionname = "+_identifier);
		System.out.println(tab+"\tArgs:");
		for(ASTnode arg : _args){
			arg.print(tab+"\t");
		}
	}
	
	@Override
	public void writeTree(BufferedWriter w) throws IOException
	{
		w.write(getID()+"");
		w.newLine();
		w.write(getID()+"[label=\"FunctionCall: "+_identifier+"\"]");
		w.newLine();
		for(ASTnode arg: _args){
			w.write(getID()+"->");
			arg.writeTree(w);
			w.newLine();
		}
	}
	
	@Override
	public void generateCode(BufferedWriter w) throws IOException{
		
	}

}
