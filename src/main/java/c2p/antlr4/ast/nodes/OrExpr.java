package main.java.c2p.antlr4.ast.nodes;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;

import c2p.antlr4.C2PParser;
import main.java.c2p.antlr4.ast.ASTnode;

public class OrExpr extends ASTnode {
	private ArrayList<ASTnode> _exprs;

	public OrExpr(ArrayList<ASTnode> exprs) {
		super(C2PParser.RULE_or_expr);
		_exprs = exprs;
	}
	
	@Override
	public void print(String tab){
		System.out.println(tab+"OrExpr:");
		for(ASTnode expr:_exprs){
			expr.print(tab+"\t");
		}
	}
	
	@Override
	public void writeTree(BufferedWriter w) throws IOException
	{
		w.write(getID()+"");
		w.newLine();
		w.write(getID()+"[label=\"Or\"]");
		for(ASTnode expr: _exprs){
			w.write(getID()+"->");
			expr.writeTree(w);
			w.newLine();
		}
	}
	
	@Override
	public void generateCode(BufferedWriter w) throws IOException{
		
	}

}
