package main.java.c2p.antlr4.ast.nodes;

import java.io.BufferedWriter;
import java.io.IOException;

import c2p.antlr4.C2PParser;
import main.java.c2p.antlr4.ast.ASTnode;

public class Statement extends ASTnode {
	ASTnode _sub;
	public Statement(ASTnode sub) {
		super(C2PParser.RULE_statement);
		_sub = sub;
	}
	
	@Override
	public void print(String tab){
		_sub.print(tab);
	}
	
	@Override
	public void writeTree(BufferedWriter w) throws IOException
	{
		_sub.writeTree(w);
	}
	
	@Override
	public void generateCode(BufferedWriter w) throws IOException{
		
	}

}
