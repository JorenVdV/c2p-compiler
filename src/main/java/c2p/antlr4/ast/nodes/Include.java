package main.java.c2p.antlr4.ast.nodes;

import java.io.BufferedWriter;
import java.io.IOException;

import main.java.c2p.antlr4.ast.ASTnode;
import main.java.c2p.antlr4.symboltable.DuplicateSymbolException;
import c2p.antlr4.C2PParser;


public class Include extends ASTnode
/*
 * ASTnode Include , contains all the information of the include statement.
 * We need only to extract the filename, and check it for being equal to stdio.h.
 * If stdio.h is included the generate code should add scanf and printf to the symboltable.
 * There is no precompiler functionality in the parser.
 */
{
	/* Filename extracted from tree */
	private String _filename;
	
	public Include(String filename) throws DuplicateSymbolException
	/*
	 * Include is an terminal node, will not have a subtree
	 */
	{
		super(C2PParser.RULE_include);
		
		_filename = filename;
		
		//check stdio
		if (_filename.equals("<stdio.h>")){
//			System.out.println("stdio detected");
//			System.out.println("Making sure Printf and Scanf are supported!");
			_symboltable.AddLibraryReserved();
		}	
	}
	
	@Override
	public void print(String tab)
	{
		System.out.println(tab+"Include: "+_filename);
	}
	
	public void printFile()
	{
		System.out.println(_filename);
	}
	
	
	@Override
	public void writeTree(BufferedWriter w) throws IOException {
		w.write(getID()+"");
		w.newLine();
		w.write(getID()+"[label = \"Include: "+_filename+"\"]");
	}
	
	@Override
	public void generateCode(BufferedWriter w) throws IOException{
		System.out.println("Generating Include-Statement Code!");
		w.write("; No code to generate for Include Statements"); 
		
	}
	
}