package main.java.c2p.antlr4.ast.nodes;

import java.io.BufferedWriter;
import java.io.IOException;

import main.java.c2p.antlr4.ast.ASTnode;

public class Characterterm extends ASTnode {
	String _char;
	public Characterterm(String charl) {
		super(31);
		_char = charl;
	}
	
	public String getVal(){
		return _char;
	}
	
	@Override
	public void print(String tab){
		System.out.println(tab+"CharacterTerm: "+_char);
	}
	
	@Override
	public void writeTree(BufferedWriter w) throws IOException
	{
		w.write(getID()+"");
		w.newLine();
		w.write(getID()+"[label=\"Character:"+_char+"\"]");
		w.newLine();
	}
	
	@Override
	public void generateCode(BufferedWriter w) throws IOException
	{
		w.write("ldc c '"+_char+"'");
	}

}
