package main.java.c2p.antlr4.ast.nodes;

import java.io.BufferedWriter;
import java.io.IOException;

import c2p.antlr4.C2PParser;
import main.java.c2p.antlr4.ast.ASTnode;
import main.java.c2p.antlr4.symboltable.Type;

public class Variable extends ASTnode{
	
	private final String _id;
	// int array_size, equal to size o array (if any else 0)
	private final int _array_size;
	// bool pointer, true if variable is of pointer type
	private final boolean _pointer;
	// bool cosnt, if true variable is const
	private final boolean _const;
	//BaseType type, determines type of variable
	private final Type _type;
	
	private Expression _expr;
	

	public Variable(String id, int array_size, boolean pointer, 
			boolean con, Type type,Expression expr) {
		super(C2PParser.RULE_variable);
		_id = id;
		_array_size = array_size;
		_pointer = pointer;
		_const = con;
		_type = type;
		_expr = expr;
	}
	
	@Override
	public void print (String tab)
	{
		System.out.println(tab+"id = "+_id);
		System.out.println(tab+"array size = "+_array_size);
		System.out.println(tab+"pointer = "+_pointer);
		System.out.println(tab+"constant = "+_const);
		System.out.println(tab+"type = "+_type.toString());
		if(_expr!=null)_expr.print(tab);
		System.out.println();
		
	}
	
	@Override
	public void writeTree(BufferedWriter w) throws IOException {
		w.write(getID()+"");
		w.newLine();
		w.write(getID()+"[label = \"Variable: "+ _id+"\"]");
		w.newLine();
		if(_expr!=null){
			w.write(getID()+"->");
			_expr.writeTree(w);
		}
	}
	
	@Override
	public String getName()
	{
		return _id;
	}
	
	public int getArraySize()
	{
		return _array_size;
	}
	
	public boolean isPointer()
	{
		return _pointer;
	}
	
	public boolean isConst()
	{
		return _const;
	}
	
	public Expression getExpression()
	{
		return _expr;
	}
	
	public Type getType(){
		return _type;
	}
	
	
	public void generateCode(BufferedWriter w) throws IOException {
		// needs to add the variable in the memory and writes it's value if possible
		// there should be a addressing table somewhere to translate variables to their address
		//TODO
	}
	
	
}
