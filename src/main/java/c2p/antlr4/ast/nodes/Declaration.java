package main.java.c2p.antlr4.ast.nodes;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;

import c2p.antlr4.C2PParser;
import main.java.c2p.antlr4.ast.ASTnode;

public class Declaration extends ASTnode {
	
	private final ArrayList<Variable> _var;
	
	public Declaration(ArrayList<Variable> children){
		super(C2PParser.RULE_declaration);
		_var = children;
		
	}
	
	@Override
	public void print(String tab)
	{
		System.out.println(tab+"Declaration: ");
		for (Variable var: _var){
			var.print(tab+"\t");
		}
	}
	
	public ArrayList<Variable> getChildren(){
		return _var;
	}
	
	@Override
	public void writeTree(BufferedWriter w) throws IOException {
		w.write(getID()+"");
		w.newLine();
		w.write(getID()+"[label = \"Declaration\"]");
		w.newLine();
		for(Variable var :_var){
			w.write(getID()+"->");
			var.writeTree(w);
			w.newLine();
		}
	}
	
	@Override
	public void generateCode(BufferedWriter w) throws IOException {
		for(Variable var : _var){
			var.generateCode(w);
		}
	}


}
