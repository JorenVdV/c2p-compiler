package main.java.c2p.antlr4.ast.nodes;

import java.io.BufferedWriter;
import java.io.IOException;

import c2p.antlr4.C2PParser;
import main.java.c2p.antlr4.ast.ASTnode;

public class Break extends ASTnode {

	public Break() {
		super(C2PParser.Break);
	}
	
	@Override
	public void print(String tab){
		System.out.println(tab+"BREAK;");
	}
	
	@Override
	public void writeTree(BufferedWriter w) throws IOException
	{
		w.write(getID()+"");
		w.newLine();
		w.write(getID()+"[label=\"Break\"]");
		w.newLine();
	}
	
	@Override
	public void generateCode(BufferedWriter w) throws IOException{
		
	}
	
	

}
