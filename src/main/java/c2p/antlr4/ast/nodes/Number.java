package main.java.c2p.antlr4.ast.nodes;

import java.io.BufferedWriter;
import java.io.IOException;

import main.java.c2p.antlr4.ast.ASTnode;
/*
 * All values that can be loaded as constants and thus
 * do not require specific memory allocation.
 */
public class Number extends ASTnode {
	
	private int _val;
	public Number(int val) {
		super(30);
		_val = val;
	}
	public int getVal() {
		return _val;
	}
	
	@Override
	public void print(String tab){
		System.out.println(tab+"Number: "+_val);
	}
	
	@Override
	public void writeTree(BufferedWriter w) throws IOException
	{
		w.write(getID()+"");
		w.newLine();
		w.write(getID()+"[label=\"Number: "+_val+"\"]");
		w.newLine();
	}
	
	@Override
	public void generateCode(BufferedWriter w) throws IOException{
		System.out.println("Generating Code for a constant LDC");
		w.write("ldc i " + _val);
	}

}
