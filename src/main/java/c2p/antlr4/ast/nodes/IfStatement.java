package main.java.c2p.antlr4.ast.nodes;

import java.io.BufferedWriter;
import java.io.IOException;

import c2p.antlr4.C2PParser;
import main.java.c2p.antlr4.ast.ASTnode;
import main.java.c2p.antlr4.ast.Labels;
import main.java.c2p.antlr4.ast.Labels.LabelType;

public class IfStatement extends ASTnode {
	
	private Expression _expression;
	private ASTnode _if_action;
	private ASTnode _else_action;

	public IfStatement(Expression expr, ASTnode if_action) {
		super(C2PParser.RULE_if_statement);
		_expression = expr;
		_if_action = if_action;
		_else_action = null;
	}
	
	public void addElseStatement(ASTnode else_action)
	{
		_else_action = else_action;
	}
	
	@Override
	public void print(String tab){
		System.out.println(tab+"If Statement:");
		System.out.println(tab+"\t If condition: ");
		_expression.print(tab+"\t");
		System.out.println(tab+"\t If action: ");
		_if_action.print(tab+"\t");
		if(_else_action!= null)System.out.println(tab+"\t Else action");
		if(_else_action!= null)_else_action.print(tab+"\t");
	}

	
	@Override
	public void writeTree(BufferedWriter w) throws IOException
	{
		w.write(getID()+"");
		w.newLine();
		w.write(getID()+"[label = \"If Statement\"]");
		w.newLine();
		w.write(getID()+"->");
		_expression.writeTree(w);;
		w.newLine();
		w.write(getID()+"->");
		_if_action.writeTree(w);
		w.newLine();
		if(_else_action!=null){
			w.write(getID()+"->");
			_else_action.writeTree(w);
			w.newLine();
		}
		
	}
	
	@Override
	public void generateCode(BufferedWriter w) throws IOException{
		String label = Labels.Label(LabelType.IF);
		_expression.generateCode(w);//Generate code for the Condition
		//Top of stack should now contain the result for the expression
		w.write("ldc b 0"); //Load Boolean False
		w.write("equ b");//sp-1 = (sp == sp-1) , rebase 
		w.write("fjp " + label + "TRUE");//If top == false (Which means equ returns true), jump label
		
		w.write(label + "TRUE:"); //If true label
		_if_action.generateCode(w); //Code for the TRUE
		w.write("ujp " + label + "OUT");//When done jump to out
		
		w.write(label + "FALSE:"); //Else
		if(_else_action != null){
			_else_action.generateCode(w); //Generate code if needed
		}
		w.write("ujp " + label + "OUT");//When done jump out
		w.write(label + "OUT:");// Continue code.
	}
}
