package main.java.c2p.antlr4.ast.nodes;

import java.io.BufferedWriter;
import java.io.IOException;

import c2p.antlr4.C2PParser;
import main.java.c2p.antlr4.ast.ASTnode;

public class MultExpr extends ASTnode {
	public enum OpMul{ DIV, MUL}
	
	private ASTnode _expr0;
	private ASTnode _expr1;
	private OpMul _op;

	public MultExpr(ASTnode expr0, ASTnode expr1, OpMul op) {
		super(C2PParser.RULE_mult_expr);
		_expr0 = expr0;
		_expr1 = expr1;
		_op = op;
	}
	
	@Override
	public void print(String tab){
		System.out.println(tab+"MultExpr:");
		switch(_op){
		case DIV:System.out.println(tab+"\t Div");
			break;
		case MUL:System.out.println(tab+"\t Mul");
			break;
		default:
			break;
		}
		_expr0.print(tab+"\t");
		_expr1.print(tab+"\t");
	}
	
	@Override
	public void writeTree(BufferedWriter w) throws IOException
	{
		w.write(getID()+"");
		w.newLine();
		if(_op.equals(OpMul.DIV))w.write(getID()+"[label=\"Div\"]");
		if(_op.equals(OpMul.MUL))w.write(getID()+"[label=\"Mul\"]");
		w.newLine();
		w.write(getID()+"->");
		_expr0.writeTree(w);
		w.newLine();
		w.write(getID()+"->");
		_expr1.writeTree(w);
		w.newLine();
	}
	
	@Override
	public void generateCode(BufferedWriter w) throws IOException{
		System.out.println("Writing code for Multiplication");
		_expr0.generateCode(w);
		_expr1.generateCode(w);
		if(_op.equals(OpMul.MUL)) w.write("mul i");
		else w.write("div i");
	}

}
