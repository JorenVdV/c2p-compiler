package main.java.c2p.antlr4.ast.nodes;

import java.io.BufferedWriter;
import java.io.IOException;

import main.java.c2p.antlr4.ast.ASTnode;

public class VariableCall extends ASTnode {
	private String _id;
	public VariableCall(String id) {
		super(34);
		_id = id;
	}
	
	
	@Override
	public void print(String tab){
		System.out.println(tab+"VariableCall: "+_id);
	}
	
	@Override
	public void writeTree(BufferedWriter w) throws IOException
	{
		w.write(getID()+"");
		w.newLine();
		w.write(getID()+"[label=\"VariableCall: "+_id+"\"]");
		w.newLine();
	}
	
	@Override
	public void generateCode(BufferedWriter w) throws IOException{
		
	}

}
