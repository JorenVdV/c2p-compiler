package main.java.c2p.antlr4.ast.nodes;

import java.io.BufferedWriter;
import java.io.IOException;

import c2p.antlr4.C2PParser;
import main.java.c2p.antlr4.ast.ASTnode;

public class ForStatement extends ASTnode {
	private Expression _expression0; // setup
	private Expression _expression1; // end condition
	private Expression _expression2; // increment
	
	private ASTnode _for_action;
	
	public ForStatement(Expression expr0, Expression expr1,
			Expression expr2, ASTnode for_action) {
		super(C2PParser.RULE_for_statement);
		_expression0 = expr0;
		_expression1 = expr1;
		_expression2 = expr2;
		_for_action = for_action;
	}
	
	@Override
	public void print(String tab){
		System.out.println(tab+"ForStatement:");
		System.out.println(tab+"\tFor: ");
		_expression0.print(tab+"\t");
		_expression1.print(tab+"\t");
		_expression2.print(tab+"\t");
		System.out.println(tab+"\t Do:");
		_for_action.print(tab+"\t");
	}
	
	@Override
	public void writeTree(BufferedWriter w) throws IOException
	{
		w.write(getID()+"");
		w.newLine();
		w.write(getID()+"[label=\"For Statement\"]");
		w.newLine();
		w.write(getID()+"->");
		_expression0.writeTree(w);
		w.newLine();
		
		w.write(getID()+"->");
		_expression1.writeTree(w);
		w.newLine();
		
		w.write(getID()+"->");
		_expression2.writeTree(w);
		w.newLine();
		
		w.write(getID()+"->");
		_for_action.writeTree(w);
		w.newLine();
	}
	
	@Override
	public void generateCode(BufferedWriter w) throws IOException{
		
	}

}
