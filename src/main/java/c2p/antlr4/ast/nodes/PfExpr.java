package main.java.c2p.antlr4.ast.nodes;

import java.io.BufferedWriter;
import java.io.IOException;

import c2p.antlr4.C2PParser;
import main.java.c2p.antlr4.ast.ASTnode;

public class PfExpr extends ASTnode {
	private ASTnode _expr;
	private String _id;

	public PfExpr(ASTnode expr, String id) {
		super(C2PParser.RULE_pf_expr);
		_expr = expr;
		_id = id;
	}
	
	@Override
	public void print(String tab){
		System.out.println(tab+"PfExpression:");
		System.out.println(tab+"\t"+_id);
		_expr.print(tab+"\t");
	}
	
	@Override
	public void writeTree(BufferedWriter w) throws IOException
	{
		w.write(getID()+"");
		w.newLine();
		w.write(getID()+"[label=\"PfExpression: "+_id+"\"]");
		w.newLine();
		w.write(getID()+"->");
		_expr.writeTree(w);
		w.newLine();
	}
	
	@Override
	public void generateCode(BufferedWriter w) throws IOException{
		
	}

}
