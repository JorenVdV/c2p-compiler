package main.java.c2p.antlr4.ast.nodes;

import java.io.BufferedWriter;
import java.io.IOException;

import c2p.antlr4.C2PParser;
import main.java.c2p.antlr4.ast.ASTnode;
import main.java.c2p.antlr4.ast.Labels;
import main.java.c2p.antlr4.ast.Labels.LabelType;

public class WhileStatement extends ASTnode {
	
	private Expression _expression;
	private ASTnode _while_action;

	public WhileStatement(Expression expr, ASTnode while_action) {
		super(C2PParser.RULE_while_statement);
		_expression = expr;
		_while_action =  while_action;
	}
	
	@Override
	public void print(String tab){
		System.out.println(tab+"While Statement");
		System.out.println(tab+"\tWhile Condition:");
		_expression.print(tab+"\t");
		System.out.println(tab+"\tWhile Action");
		_while_action.print(tab+"\t");
	}
	
	@Override
	public void writeTree(BufferedWriter w) throws IOException
	{
		w.write(getID()+"");
		w.newLine();
		w.write(getID()+"[label=\"While Statement\"]");
		w.newLine();
		
		w.write(getID()+"->");
		_expression.writeTree(w);
		w.newLine();
		
		w.write(getID()+"->");
		_while_action.writeTree(w);
		w.newLine();
	}
	
	@Override
	public void generateCode(BufferedWriter w) throws IOException{
		if(_expression != null){
			String label = Labels.Label(LabelType.WHILE);
			w.write(label + "CONDITION:");//label the condition code
			_expression.generateCode(w);//Condition Code
			w.write("ldc b 0");//LOAD False
			w.write("equ b");//If sp-1 == sp1, both false jump true
			w.write("fjp " + label + "TRUE");
			w.write("ujp " + label + "OUT");
			
			if(_while_action != null){
				w.write(label + "TRUE:");//Label the while action code
				_while_action.generateCode(w);//While action code
				w.write("ujp " + label + "CONDITION");//Check if while still true
			}
			w.write(label + "OUT:");
		}
	}

}
