package main.java.c2p.antlr4.ast.nodes;

import java.io.BufferedWriter;
import java.io.IOException;

import c2p.antlr4.C2PParser;
import main.java.c2p.antlr4.ast.ASTnode;

public class ReturnStatement extends ASTnode {
	private Expression _expression;
	
	public ReturnStatement(Expression expr) {
		super(C2PParser.RULE_return_statement);
		_expression = expr;
	}
	
	@Override
	public void print(String tab){
		System.out.println(tab+"Return Statement");
		_expression.print(tab+"\t");
	}
	
	@Override
	public void writeTree(BufferedWriter w) throws IOException
	{
		w.write(getID()+"");
		w.newLine();
		w.write(getID()+"[label=\"Return Statement\"]");
		w.newLine();
		if(_expression!=null)w.write(getID()+"->");
		if(_expression!=null)_expression.writeTree(w);
	}
	
	@Override
	public void generateCode(BufferedWriter w) throws IOException{
		
	}

}
