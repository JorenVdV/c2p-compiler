package main.java.c2p.antlr4.ast.nodes;

import java.io.BufferedWriter;
import java.io.IOException;

import main.java.c2p.antlr4.ast.ASTnode;

public class Stringterm extends ASTnode {
	private String _string;
	public Stringterm(String str) {
		super(40);
		_string = str;
	}
	public String getVal() {
		return _string;
	}
	
	@Override
	public void print(String tab){
		System.out.println(tab+"String = "+_string);
	}
	
	@Override
	public void writeTree(BufferedWriter w) throws IOException
	{
		String temp = _string;
		if(temp.charAt(0)=='"')temp = temp.substring(1);
		if(temp.charAt(temp.length()-1)=='"')temp = temp.substring(0, temp.length()-1);
		w.write(getID()+"");
		w.newLine();
		w.write(getID()+"[label=\"String: "+temp+"\"]");
		w.newLine();
	}
	
	@Override
	public void generateCode(BufferedWriter w) throws IOException{
		
	}

}
