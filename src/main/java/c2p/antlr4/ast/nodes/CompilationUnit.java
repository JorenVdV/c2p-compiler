package main.java.c2p.antlr4.ast.nodes;


import c2p.antlr4.C2PParser;
import main.java.c2p.antlr4.ast.ASTnode;
import java.util.ArrayList;
import java.util.List;
import java.io.*;



public class CompilationUnit extends ASTnode
/*
 * ASTnode CompilationUnit, most toplevel node, will follow up on the parse node
 * Contains all includes, variable declarations and function declaration
 * generating the code of the CompilationUnit will be responsible for generating
 * the entire P code compiled from the given input
 */
{
	/* Abstract Syntax tree to be used in functions */
	//private AST _tree;
	/* All includes contained in the .c file */
	private List<Include> _includes = new ArrayList<Include>();
	/* All global variable declarations found in the .c file */
	private List<Declaration> _var_decl = new ArrayList<Declaration>();
	/* All function declarations/ definitions found in the .c file */
	private List<FunctionDecl> _func_decl = new ArrayList<FunctionDecl>();
	
	
	public CompilationUnit(){
		super(C2PParser.RULE_compilation_unit);
	}
	
	public void addIncludes(List<Include> incl)
	{
		_includes.addAll(incl);
	}
	
	public void addVarDecl(List<Declaration> vd)
	{
		_var_decl.addAll(vd);
	}
	
	public void addFuncDecl(List<FunctionDecl> fd)
	{
		_func_decl.addAll(fd);
	}
	
	@Override
	public void print(String tab)
	{
		System.out.println("Compilation Unit:");
		for (Include incl:_includes)
		/*
		 * print all includes 
		 */
		{
			incl.print(tab+"\t");
		}
		for (Declaration decl: _var_decl)
		/*
		 * print all Declarations
		 */
		{
			decl.print(tab+"\t");
		}
		for (FunctionDecl func: _func_decl)
		/*
		 * print all Function declaration
		 */
		{
			func.print(tab+"\t");
		}
	}
	
	@Override
	public void writeTree(BufferedWriter w) throws IOException
	// Writes the Compilation Unit to a graphviz tree
	{
		w.write("digraph G {");
		w.newLine();
		w.write(getID()+"[label=\"Compilation Unit\"]");
		w.newLine();
		for(Include incl : _includes){
			w.write(getID()+"->");
			incl.writeTree(w);
			w.newLine();
		}
		for(Declaration decl : _var_decl){
			w.write(getID()+"->");
			decl.writeTree(w);
			w.newLine();
		}
		for(FunctionDecl func: _func_decl){
			w.write(getID()+"->");
			func.writeTree(w);
			w.newLine();
		}
		
		w.write("}");
		w.newLine();
	}
	
	
	@Override
	public void generateCode(BufferedWriter w) throws IOException
	/* @param w : Writer which has been initialized to the correct file to write to
	 * Generates the entire code for the entire compilation unit by calling all children's Generate Code
	 * do all includes, Var, and Funcdecls
	 */
	{
//		System.out.println("Initializing Code Generation for P.");
//		for(Include incl : _includes){
//			incl.generateCode(w);
//		} no need for this?
		// ssp count variables in scope
		w.write("ssp "+ASTnode._symboltable.getScope(getID()).getNOV());
		
		for(Declaration decl : _var_decl){
			decl.generateCode(w);
		}
		
		for(FunctionDecl fdecl : _func_decl){
			try {
				fdecl.generateCode(w);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			
	}
}