package main.java.c2p.antlr4.ast;

public class Labels {
	/*
	 * Gives us a way to generate unique labels
	 * as we need to know where to jump
	 */
	private static int if_labels = 0;
	private static int while_labels = 0;
	private static int for_labels = 0;
	private static int unknown_labels = 0;
	
	public enum LabelType {
		IF, WHILE, FOR, UNKNOWN
	}
	
	public Labels() {
		// TODO Auto-generated constructor stub
	}
	
	public static String Label(LabelType label){
		if(label.equals(LabelType.IF)){
			String newlabel = "IF[" + if_labels + "]";
			if_labels++;
			return newlabel;
		}
		else if(label.equals(LabelType.WHILE)){
			String newlabel = "WHILE[" + while_labels + "]";
			while_labels++;
			return newlabel;
		}
		else{
			String newlabel = "UNKNOWN[" + unknown_labels + "]";
			unknown_labels++;
			return newlabel;
		}
		
	}
}
