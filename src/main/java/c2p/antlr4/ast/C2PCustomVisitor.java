package main.java.c2p.antlr4.ast;

import java.util.ArrayList;
import java.util.Vector;

import main.java.c2p.antlr4.symboltable.DuplicateSymbolException;
import main.java.c2p.antlr4.symboltable.FunctionSymbol;
import main.java.c2p.antlr4.symboltable.Pair;
import main.java.c2p.antlr4.symboltable.Symbol;
import main.java.c2p.antlr4.symboltable.Type;
import main.java.c2p.antlr4.symboltable.Type.BaseType;
import main.java.c2p.antlr4.symboltable.VariableSymbol;
import main.java.c2p.antlr4.ast.nodes.*;
import main.java.c2p.antlr4.ast.nodes.AddExpr.OpAdd;
import main.java.c2p.antlr4.ast.nodes.MultExpr.OpMul;
import main.java.c2p.antlr4.ast.nodes.Number;
import main.java.c2p.antlr4.ast.nodes.RelExpr.Rel;

import org.antlr.v4.runtime.tree.ParseTree;

import c2p.antlr4.C2PParser.Add_exprContext;
import c2p.antlr4.C2PParser.And_exprContext;
import c2p.antlr4.C2PParser.Array_indexContext;
import c2p.antlr4.C2PParser.BlockContext;
import c2p.antlr4.C2PParser.Compilation_unitContext;
import c2p.antlr4.C2PParser.Complex_declContext;
import c2p.antlr4.C2PParser.DeclarationContext;
import c2p.antlr4.C2PParser.ExpressionContext;
import c2p.antlr4.C2PParser.For_statementContext;
import c2p.antlr4.C2PParser.Function_declarationContext;
import c2p.antlr4.C2PParser.If_statementContext;
import c2p.antlr4.C2PParser.IncludeContext;
import c2p.antlr4.C2PParser.Mult_exprContext;
import c2p.antlr4.C2PParser.Or_exprContext;
import c2p.antlr4.C2PParser.ParameterContext;
import c2p.antlr4.C2PParser.Parameter_listContext;
import c2p.antlr4.C2PParser.Pf_exprContext;
import c2p.antlr4.C2PParser.Pr_exprContext;
import c2p.antlr4.C2PParser.Rel_exprContext;
import c2p.antlr4.C2PParser.Return_statementContext;
import c2p.antlr4.C2PParser.StatementContext;
import c2p.antlr4.C2PParser.TypeContext;
import c2p.antlr4.C2PParser.Unary_exprContext;
import c2p.antlr4.C2PParser.VariableContext;
import c2p.antlr4.C2PParser.While_statementContext;

public class C2PCustomVisitor {

	public ASTnode visit(ParseTree arg0)
	/**
	 * Most toplevel function, will be called upon to generate the ASTnodes
	 */
	{
		try {
			return visitCompilation_unit((Compilation_unitContext) arg0
					.getChild(0));
		} catch (Exception e) {
			System.err.println("Error: parsing failed");
			e.printStackTrace();
			return null;
		}
	}

	public CompilationUnit visitCompilation_unit(Compilation_unitContext ctx)
			throws Exception
	/**
	 * Visits the compilation unit, all the includes, global variable
	 * declaration and function declaration will be children of the node this
	 * function generates
	 */
	{
		/*
		 * list used for storing all the subnodes
		 */
		ArrayList<Include> includes = new ArrayList<Include>();
		ArrayList<Declaration> vdecls = new ArrayList<Declaration>();
		ArrayList<FunctionDecl> fdecls = new ArrayList<FunctionDecl>();

		CompilationUnit comp = new CompilationUnit();

		ASTnode._symboltable.AddNewScope(comp.getID());

		for (IncludeContext include : ctx.include())
		/*
		 * loop over all includes add all include nodes to the includes list
		 */
		{
			includes.add(visitInclude(include));
		}
		comp.addIncludes(includes);

		for (DeclarationContext vdecl : ctx.declaration())
		/*
		 * loop over all variable declaration add all declaration nodes to the
		 * include list
		 */
		{
			vdecls.add(visitDeclaration(vdecl));
		}
		comp.addVarDecl(vdecls);

		for (Function_declarationContext fdecl : ctx.function_declaration()) {
			fdecls.add(visitFunction_declaration(fdecl));
		}
		comp.addFuncDecl(fdecls);

		return comp;
	}

	public Include visitInclude(IncludeContext ctx) throws DuplicateSymbolException
	/**
	 * This function will visit the parse tree version of the include and return
	 * the ASTnode include, containing the filename.
	 */
	{
		/*
		 * retrieve filename from the tree filename is merged to 1 string and
		 * given to the include node constructor
		 */
		String includestring = ctx.getChild(1).getText() + ctx.file.getText()
				+ "." + ctx.extension.getText() + ctx.getChild(5).getText();
		/*
		 * Construct ASTnode include This node will be the return value of the
		 * function
		 */
		return new Include(includestring);
	}

	public Declaration visitDeclaration(DeclarationContext ctx)
			throws Exception {
		ArrayList<Variable> variables = new ArrayList<Variable>();

		/*
		 * derive type
		 */
		TypeContext tc = ctx.type();
		Type t = new Type(BaseType.VOID);
		if (tc.Char() != null)
			t = new Type(BaseType.CHAR);
		else if (tc.Int() != null)
			t = new Type(BaseType.INT);
		else if (tc.Void() != null)
			t = new Type(BaseType.VOID);
		else {
			System.err.println("Type not found at" + tc.t.getLine() + ":"
					+ tc.t.getCharPositionInLine());
			throw new Exception();
		}
		Boolean constant = tc.Const() != null;

		/*
		 * derive complex_declaration
		 */
		Complex_declContext cd = ctx.complex_decl();

		for (VariableContext vc : cd.variable())
		{
			Variable var = visitVariable(vc, t, constant);//new Variable(id, array, pointer, constant, t, e);
			variables.add(var);
			ASTnode._symboltable.AddSymbol(var);

		}
		return new Declaration(variables);
	}
	
	public Variable visitVariable(VariableContext ctx, Type t, boolean con) throws Exception
	{
		String variable_name = ctx.variable_id().Identifier().getText();
		
		int array_size=0;
		if(ctx.variable_id().array_index() != null){
			if(ctx.variable_id().array_index().Number()!=null){
				// we have a simple integer as array size
				array_size = Integer.parseInt(ctx.variable_id().array_index().Number().getText());
			}else if (ctx.variable_id().array_index().Identifier() != null){
				ASTnode vs = ASTnode._symboltable.LookUp(ctx.variable_id().array_index().Identifier().getText());
				if(vs.getClass().equals(Variable.class)){
					Variable var = (Variable) vs;
					if(var.getType().getType().equals(BaseType.INT)){
						if (var.isConst() && !var.isPointer()){
							if(var.getExpression().isSingleVal()){
								UnaryExpr ue = (UnaryExpr) var.getExpression().getRvalue();
								if(ue.getChild().getClass().equals(Number.class)){
									array_size =((Number) ue.getChild()).getVal();
								}else{
									System.err.println("Something went wrong @"+ctx.start.getLine());
								}
							}else{
								System.err.println("Variable has no direct value, needed for array initialization @"+ ctx.start.getLine());
								throw new Exception();
							}
						}else{
							System.err.println("Expected const no pointer variable @"+ctx.start.getLine());
							throw new Exception();
						}
					}else{
						System.err.println("Variable of wrong type, expected int got "+ var.getType().toString()
								+" @"+ctx.start.getLine());
						throw new Exception();
					}
				}else{
					System.err.println("Symbol of wrong class expected Variable but got "+vs.getClass().getName()
							+" @"+ctx.start.getLine());
					throw new Exception();
				}
			}else {
				System.err.println("No possible index for array size was found @:" + 
						ctx.variable_id().array_index().start.getLine());
				throw new Exception();
			}
		}
		
		boolean ptr = (ctx.Star()!=null);
		
		Expression expr = null;
		if(ctx.expression()!=null)expr=visitExpression(ctx.expression());
		
		return new Variable(variable_name, array_size, ptr, con, t, expr);
	}

	public FunctionDecl visitFunction_declaration(
			Function_declarationContext ctx) throws Exception
	/**
	 * Creates a new function in the tree, Function will be added to the symbol
	 * table
	 */
	{
		TypeContext tc = ctx.type();
		Type t = new Type(BaseType.VOID);
		if (tc.Char() != null)
			t = new Type(BaseType.CHAR);
		else if (tc.Int() != null)
			t = new Type(BaseType.INT);
		else if (tc.Void() != null)
			t = new Type(BaseType.VOID);
		else {
			System.err.println("Type not found at" + tc.t.getLine() + ":"
					+ tc.t.getCharPositionInLine());
			throw new Exception();
		}
		Boolean constant = tc.Const() != null;
		String function_name = ctx.funcname.getText();

		// Retrieve parameter list
		Vector<Pair<Type, String>> plist = visitParameter_list(ctx.plist);

		// insert function into symbol table for recursive calls
		FunctionDecl f = new FunctionDecl(function_name, t, constant, plist);

		ASTnode._symboltable.AddSymbol(f);

		Block block = visitBlock(ctx.blck);
		if (block != null)
			f.addBlock(block);
		return f;
	}

	public Vector<Pair<Type, String>> visitParameter_list(
			Parameter_listContext ctx) throws DuplicateSymbolException {
		Vector<Pair<Type, String>> parameterlist = new Vector<Pair<Type, String>>();
		if (ctx.Void() != null || ctx.parameter().size() == 0) 
		// void parameter
		{
			parameterlist.add(new Pair<Type, String>(new Type(BaseType.VOID),""));
		} else {
			for (ParameterContext param : ctx.parameter()) {
				Type pt = new Type(BaseType.VOID);
				if(param.type().Void() != null) pt = new Type(BaseType.VOID);
				else if (param.type().Int() != null) pt = new Type(BaseType.INT);
				else if (param.type().Char() != null ) pt = new Type(BaseType.CHAR);
				parameterlist.add(new Pair<Type, String>(pt, param.Identifier().getText()));
				ASTnode._symboltable.AddSymbol(
						new Variable(param.Identifier().getText(), 0, false, false, pt,null));
			}
		}
		return parameterlist;
	}

	public Block visitBlock(BlockContext ctx) throws Exception {
		Block block = new Block();
		ASTnode._symboltable.AddNewScope(block.getID());
		ArrayList<Statement> statements = new ArrayList<Statement>();
		for (StatementContext statement : ctx.statement()) {
			statements.add(visitStatement(statement));
		}
		block.addStatements(statements);
		ASTnode._symboltable.LeaveScope();
		return block;
	}

	public Statement visitStatement(StatementContext ctx) throws Exception {
		ASTnode sub = null;
		if (ctx.if_statement() != null)
			sub = visitIf_statement(ctx.if_statement());
		else if (ctx.while_statement()!= null)
			sub = visitWhile_statement(ctx.while_statement());
		else if (ctx.for_statement() != null)
			sub = visitFor_statement(ctx.for_statement());
		else if (ctx.expression() != null)
			sub = visitExpression(ctx.expression());
		else if (ctx.return_statement() != null)
			sub = visitReturn_statement(ctx.return_statement());
		else if (ctx.declaration() != null)
			sub = visitDeclaration(ctx.declaration());
		return new Statement(sub);
	}

	public IfStatement visitIf_statement(If_statementContext ctx)
			throws Exception {
		Expression expr = visitExpression(ctx.expression());

		// determine if action (continue| break | statement | block)
		ASTnode if_action = null;
		if (ctx.Continue() != null)
		// case where if is followed by a continue statement
		{
			if_action = new Continue();
		} 
		else if (ctx.Break() != null)
		// case where if is followed by a break statement
		{
			if_action = new Break();
		} 
		else if (ctx.block() != null)
		// case where if is followed by a block
		{
			if_action = visitBlock(ctx.block());
		} 
		else if (ctx.statement() != null)
		{
			if_action = visitStatement(ctx.statement());
		}else{
			System.err.println("Error in if statement at line "+ctx.start.getLine());
			throw new Exception();
		}

		IfStatement if_statement = new IfStatement(expr, if_action);

		// parse else statement
		if (ctx.else_statement() != null) {
			ASTnode else_action = null;
			if (ctx.else_statement().Continue() != null) {
				else_action = new Continue();
			} else if (ctx.else_statement().Break()!= null) {
				else_action = new Break();
			} else if (ctx.else_statement().block() != null) {
				else_action = visitBlock(ctx.else_statement().block());
			} else if (ctx.else_statement().statement() != null){
				else_action = visitStatement(ctx.else_statement().statement());
			}
			if_statement.addElseStatement(else_action);
		}
		return if_statement;
	}

	public WhileStatement visitWhile_statement(While_statementContext ctx)
			throws Exception {
		
		Expression expr = visitExpression(ctx.expression());
		ASTnode while_action = null;
		
		if (ctx.block() != null) {
			while_action = visitBlock(ctx.block());
		} else if (ctx.statement() != null){
			while_action = visitStatement(ctx.statement());
		} else {
			System.err.println("Error: no viable action for while loop found at "+ ctx.start.getLine());
			throw new Exception();
		}

		return new WhileStatement(expr, while_action);
	}

	public ForStatement visitFor_statement(For_statementContext ctx)
			throws Exception {
		if(ctx.expression().size()!= 3){
			System.err.println("Error: no viable setup in for statement at "+ctx.start.getLine());
			throw new Exception();
		}
		Expression expr0 = visitExpression(ctx.expression(0));
		Expression expr1 = visitExpression(ctx.expression(1));
		Expression expr2 = visitExpression(ctx.expression(2));
		
		ASTnode for_action = null;
		if (ctx.block()!= null) {
			for_action = visitBlock(ctx.block());
		} else if(ctx.statement() != null){
			for_action = visitStatement(ctx.statement());
		} else {
			System.err.println("Error no viable action in for statement at "+ ctx.start.getLine());
			throw new Exception();
		}
		
		return new ForStatement(expr0, expr1, expr2, for_action);
	}

	public ReturnStatement visitReturn_statement(Return_statementContext ctx) throws Exception {
		return new ReturnStatement(visitExpression(ctx.expression()));
	}

	public Expression visitExpression(ExpressionContext ctx) throws Exception {
		ASTnode rvalue = visitOr_expr(ctx.or_expr());
		Expression expr = new Expression(rvalue);
		if (ctx.pf_expr() != null)
		// lvalue to parse
		{
			expr.addLvalue(visitPf_expr(ctx.pf_expr()));
		}
		return expr;
	}

	public ASTnode visitOr_expr(Or_exprContext ctx) throws Exception {
		if (ctx.and_expr().size() == 1)
		// single element
		{
			return visitAnd_expr(ctx.and_expr(0));
		} 
		else
		// multiple elements of the or statement
		{
			ArrayList<ASTnode> and_exprs = new ArrayList<ASTnode>();
			for (And_exprContext and : ctx.and_expr()) {
				and_exprs.add(visitAnd_expr(and));
			}
			return new OrExpr(and_exprs);
		}
	}

	public ASTnode visitAnd_expr(And_exprContext ctx) throws Exception {
		if (ctx.rel_expr().size() == 1)
		// single element
		{
			return visitRel_expr(ctx.rel_expr(0));
		} else
		// multiple elements in the and statement
		{
			ArrayList<ASTnode> exprs = new ArrayList<ASTnode>();
			for (Rel_exprContext rel : ctx.rel_expr()) {
				exprs.add(visitRel_expr(rel));
			}
			return new AndExpr(exprs);
		}
	}

	public ASTnode visitRel_expr(Rel_exprContext ctx) throws Exception {
		if (ctx.add_expr().size() == 1)
		// single element
		{
			return visitAdd_expr(ctx.add_expr(0));
		} else
		// 2 elements with relational operator in the middle
		{
			ASTnode expr0 = visitAdd_expr(ctx.add_expr(0));
			ASTnode expr1 = visitAdd_expr(ctx.add_expr(1));
			Rel rel = null;
			if (ctx.Less() != null)
				rel = Rel.LESS;
			else if (ctx.LessEqual() != null)
				rel = Rel.LESSEQ;
			else if (ctx.Greater() != null)
				rel = Rel.GREATER;
			else if (ctx.GreaterEqual() != null)
				rel = Rel.GREATEREQ;
			else if (ctx.Equal() != null)
				rel = Rel.EQ;
			else if (ctx.NotEqual() != null)
				rel = Rel.NEQ;

			return new RelExpr(expr0, expr1, rel);
		}
	}

	public ASTnode visitAdd_expr(Add_exprContext ctx) throws Exception {
		if (ctx.add_expr() == null)
		// single element
		{
			return visitMult_expr(ctx.mult_expr());
		} else
		// multiple elements
		{
			ASTnode expr0 = visitMult_expr(ctx.mult_expr());
			ASTnode expr1 = visitAdd_expr(ctx.add_expr());
			OpAdd op = null;
			if (ctx.Plus() != null)
				op = OpAdd.PLUS;
			else
				op = OpAdd.MINUS;
			return new AddExpr(expr0, expr1, op);
		}
	}

	public ASTnode visitMult_expr(Mult_exprContext ctx) throws Exception {
		if (ctx.mult_expr() == null)
		// single element
		{
			return visitUnary_expr(ctx.unary_expr());
		} else
		// multiple elements
		{
			ASTnode expr0 = visitUnary_expr(ctx.unary_expr());
			ASTnode expr1 = visitMult_expr(ctx.mult_expr());
			OpMul op = null;
			if (ctx.Div() != null)
				op = OpMul.DIV;
			else
				op = OpMul.MUL;
			return new MultExpr(expr0, expr1, op);
		}
	}

	public ASTnode visitUnary_expr(Unary_exprContext ctx) throws Exception {
		boolean minus = ctx.Minus() != null;
		boolean not = ctx.Not() != null;
		ASTnode expr = visitPf_expr(ctx.pf_expr());
		return new UnaryExpr(expr, minus, not);
	}

	public ASTnode visitPf_expr(Pf_exprContext ctx) throws Exception {
		if (ctx.pr_expr() != null) {
			// pr_expr primary expression
			return visitPr_expr(ctx.pr_expr());

		} else if (ctx.pf_expr() != null) {
			// pf_expr.identifier struct part
			ASTnode expr0 = visitPf_expr(ctx.pf_expr());
			String id = ctx.Identifier().toString();
			return new PfExpr(expr0, id);

		} else if (ctx.arg_list() != null) {
			// identifier(arg_list) functioncall
			String func_name = ctx.Identifier().getText();
			if (ASTnode._symboltable.LookUp(func_name) == null) {
				// TODO throw exception
			}
			ArrayList<ASTnode> args = new ArrayList<ASTnode>();
			for (ExpressionContext expr : ctx.arg_list().expression()) {
				args.add(visitExpression(expr));
			}
			return new FunctionCall(func_name, args);
		} else
			return null;
	}

	public ASTnode visitPr_expr(Pr_exprContext ctx) throws Exception {
		if (ctx.Identifier() != null) {
			if(!ASTnode._symboltable.LookUp(ctx.Identifier().getText()).getClass().equals(Variable.class))throw new Exception();
			
			Variable var = (Variable) ASTnode._symboltable.LookUp(ctx.Identifier().getText());
			if (var != null && var.getExpression().isSingleVal()) {
				switch (var.getType().getType()) {
				case CHAR: {
					return new Characterterm(
							((Characterterm)
							((UnaryExpr) var.getExpression().getRvalue())
							.getChild()).getVal());
				}
				case INT: {
					return new Number(
							((Number)((UnaryExpr) var.getExpression().getRvalue())
							.getChild()).getVal());
				}
				case VOID: {
					return null;
				}
				default:
					break;
				}
			} else {
				return new VariableCall(ctx.Identifier().getText());
			}

		} else if (ctx.Number() != null) {
			return new Number(Integer.parseInt(ctx.Number().getText()));

//		} else if (ctx.String() != null) {
//			return new Stringterm(ctx.String().getText());
		} else if (ctx.Character() != null) {
			return new Characterterm(ctx.Character().getText());
		} else if (ctx.expression() != null) {
			return visitExpression(ctx.expression());
		} else if (ctx.arg_list() != null) {
			ArrayList<ASTnode> args = new ArrayList<ASTnode>();
			for (ExpressionContext expr : ctx.arg_list().expression()) {
				args.add(visitExpression(expr));
			}
			return new ArgList(args);
		}
		return null;
	}

}
